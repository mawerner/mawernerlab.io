class browsingNavigator{

    /**
     * Manage vertical browsing navigation bar
     * @param {object} slider           dictionary mapping id of slider element to anchor id
     * @param {number} topMargin        offset from top of viewport in px where slider elements will stick
     * @param {number} bottomMargin     offset from bottom of viewport in px where slider elements will stick
     */
    constructor(slider, topMargin, bottomMargin) {
        
        /*  slider should hold heading elements as keys,
            map to anchor, min/max offset and targetPosition */
        this.slider = {};

        /* height of elements is required to compute offset from top-/bottomMargin */
        let sliderHeight = document.getElementById(Object.keys(slider)[0]).clientHeight;
        
        /* temporary array to rank the location of the slider */
        var arr_temp = [];

        for (let key in slider) {
            /* introduce anchor and targetPosition */
            this.slider[key] = {"anchor": slider[key]};
            this.slider[key].targetPosition = document.getElementById(this.slider[key].anchor).offsetTop;
            arr_temp[arr_temp.length] = this.slider[key].targetPosition;
        }

        /* sort temporary array in ascending order of offsetTop */
        arr_temp.sort(function(a, b){return a - b});

        for (let key in this.slider) {
            /* assign min/max offset to each slider */
            let rank = arr_temp.indexOf(this.slider[key].targetPosition);
            this.slider[key].maximal_offset = window.innerHeight - sliderHeight*(Object.keys(this.slider).length - rank) - bottomMargin;
            this.slider[key].minimal_offset = sliderHeight*rank + topMargin;
        }
    }

    /**
     * method to call in onscroll to compute positions of slider elements
     */
    browse() {

        for (let key in this.slider) {

            /* get current anchor offset in viewport */
            let viewportAnchorOffset = document.getElementById(this.slider[key].anchor).getBoundingClientRect().top;

            if (viewportAnchorOffset <= this.slider[key].minimal_offset) {
                /* if slider anchor is above screen -> fix slider to top */
                document.getElementById(key).style.top = this.slider[key].minimal_offset + "px";
                document.getElementById(key).style.position = "fixed";
            }
            else if (viewportAnchorOffset >= this.slider[key].maximal_offset){
                /* if slider anchor is below screen -> fix slider to bottom */
                document.getElementById(key).style.top = this.slider[key].maximal_offset + "px";
                document.getElementById(key).style.position = "fixed";
            }
            else {
                /* if slider anchor is onscreen -> fix to anchor */
                document.getElementById(key).style.top = this.slider[key].targetPosition + "px";
                document.getElementById(key).style.position = "absolute";
            }
        }
    }
}
